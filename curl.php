<?php

$curl = curl_init();
curl_setopt_array($curl, array(
	CURLOPT_RETURNTRANSFER => 1,
	CURLOPT_URL => 'http://dev.brackethacker.com/ping.php',
	CURLOPT_USERAGENT => 'Curl Test Request',
));

$response = curl_exec($curl);
curl_close($curl);
if(!$response) {
	die('Error: "' . curl_error($curl) . '" - Code: ' . curl_errno($curl));	
} else {
	echo $response;	
}

?>